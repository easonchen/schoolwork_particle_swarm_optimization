clear;clc;close all;
x=-4.5:0.01:4.5;y=-4.5:0.01:4.5;
for i=1:1: length(x)
    for j=1:1:length(y)
% z(i,j)=x(i)^2-10*cos(2*pi*x(i))+10+ y(j)^2-10*cos(2*pi*y(j))+10;    % 範例        
z(i,j)=(1.5-x(i)+x(i)*y(j))^2+(2.25-x(i)+x(i)*y(j)^2)^2+(2.625-x(i)+x(i)*y(j)^3)^2; % 作業
    end
end

load final_pop.ga;
figure;contour(x,y,z);hold on;
plot(final_pop(:,1), final_pop(:,2),'r*');
legend('成本函數等高線圖','最終群體位置')
title('GA解空間');xlabel('變數x');ylabel('變數y');
grid on;set(gca,'fontsize',20);axis equal;
% figure;meshc(x,y,z);axis tight;
% title('Beale function');xlabel('x'); ylabel('y');zlabel('f(x,y)');
% figure;ezsurfc('x1^2-10*cos(2*pi*x1)+10+ x2^2-10*cos(2*pi*x2)+10',[-10 10]);
% ezsurfc('z');
% 程式結束