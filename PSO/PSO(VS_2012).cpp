// PSO(VS_2012).cpp : 定義主控台應用程式的進入點。
//

// constriction PSO
// function optimization
//  find the minimum of x1^2+x2^2+x3^2+....+xn^2
//  n = _lparticle 
// optimal solution (x1,....,xn)=(0,...,0)

#include "stdafx.h"

#include  <iostream>
#include  <conio.h>
#include  <stdlib.h>
#include  <math.h>
#include  <stdio.h>
#include  <string.h>
#include  <time.h>

#define   Cost_name   "cost.dat"
#define   Init_swarm_name      "initial_position.dat"
#define   Final_swarm_name     "final_position.dat"


using namespace std;

#define   _swarmsize    50   // swarm size (number of particles)
#define   _max_ite    1000  // maximum iteration number

#define   _C1  2.0
#define   _C2  2.0
#define   _chi 0.8
#define  _range 4.5   // search domain of input variables 

const int _lparticle =  2 ;  // dimension of position vector (number of optimized variables)

class  Swarm
{
   friend void  movement(int,int *) ;
   friend void  report_save_fun(float,float,float,char *) ;
   friend void  Save(void) ;

   public :
   float   cost_fun,pbest_fun ;
   float   position[_lparticle+1],v[_lparticle+1],pbest[_lparticle+1] ;
   
   Swarm() ; 
   void    initswarm(void) ;
   void    Save_variable(FILE *ft) ;
   void    objfunc(float) ;
    
}  ;

Swarm :: Swarm()
{
   int i ;
   for(i=1;i<=_lparticle;i++)
      v[i] = 0 ;
}

void  initialize(void) ;
void  statistics(float *,float *,float *, float, int,int*) ;
void  movement(int, int *) ;
void  report_save_fun(float,float,float,char *) ;
float Eva_cost_fun(int) ;

Swarm   _newswarm[_swarmsize+1]  ; 
float  _gbest[_lparticle+1], _gbest_fun ;

int main(int argc, char* argv[])
{
	int   min, _ite ;
 void Save(void) ;
 time_t t;
  
  srand((unsigned) time(&t));

 initialize() ;

 for(_ite=1; _ite <= _max_ite;_ite++)
 {
  cout << "\n Iteration is --- " << _ite ;
  movement(_ite,&min) ;

 }
 
  cout << "\n *******  The  End ****** " ;
 
  Save() ;
  //getch();
//system("pause");
	return 0;
}

void initialize()
{
  int i,j , best_particle,min;

  float fmax_fun=-1E7, fmin_fun= 1E7, ave_fun=0 ,fun ;
  FILE *fm  ;
  errno_t err;

 cout << "\n Initial swarm " << endl ;
 
  for(j=1;j<=_swarmsize;j++)
  {   _newswarm[j].initswarm();
     
  }
  
  for(j=1;j<=_swarmsize;j++)
  {
      fun =  Eva_cost_fun(j) ;

     _newswarm[j].objfunc(fun) ;

    statistics(&fmax_fun,&fmin_fun,&ave_fun,_newswarm[j].cost_fun,j,&min) ;
	 
	 _newswarm[j].pbest_fun = _newswarm[j].cost_fun ;

  for(i=1;i<=_lparticle;i++)
    _newswarm[j].pbest[i] = _newswarm[j].position[i] ;

  }

  _gbest_fun = _newswarm[min].cost_fun ;
  
  for(i=1;i<=_lparticle;i++)
    _gbest[i] = _newswarm[min].position[i] ;

  
  if( (err=fopen_s(&fm, Init_swarm_name,"w")) != 0) exit(1) ;

  for(j=1;j<=_swarmsize;j++)
  {  _newswarm[j].Save_variable(fm) ;
  
  }

 fclose(fm) ;

  report_save_fun(fmax_fun,_gbest_fun,ave_fun,Cost_name) ;
    
}


float Eva_cost_fun(int jj)
{
  int i ;
  float fun ;

    fun=pow(1.5- _newswarm[jj].position[1]+ _newswarm[jj].position[1]* _newswarm[jj].position[2],2)
	  + pow(2.25 - _newswarm[jj].position[1] + _newswarm[jj].position[1] * pow(_newswarm[jj].position[2],2), 2)
	  + pow(2.625 - _newswarm[jj].position[1] + _newswarm[jj].position[1] * pow(_newswarm[jj].position[2], 3), 2);

//  fun = 0 ;
//  for(i=1;i<=_lparticle;i++)
//    fun = fun + _newswarm[jj].position[i]*_newswarm[jj].position[i] ;

  return fun ;

}

void Swarm :: initswarm()
{
 int i ;
 
 for(i=1;i<=_lparticle;i++)
  position[i] = _range*(rand()%2001 - 1000)/1000. ;

}

void Swarm :: objfunc(float fun)
{

  cost_fun = fun ;

}

void   statistics(float *max_fun,float *min_fun,float *ave_fun,float c_fun,int j,int *min)
{

 if ( c_fun > *max_fun )  { *max_fun = c_fun ; }

 if ( c_fun < *min_fun )  {*min_fun = c_fun ; *min =j ; }

 *ave_fun = *ave_fun + (1.0/j)*(c_fun - *ave_fun)  ;
 
}

void movement(int _ite,int *min)
{
int   i,j ;
float  fmax_fun=-1E8,fmin_fun=1E8, fun,ave_fun =0  ;

 for (i=1;i<=_swarmsize;i++)
 { for (j=1;j<=_lparticle;j++)
   { _newswarm[i].v[j] = _chi*(_newswarm[i].v[j]
     + _C1*(rand()%101)/100.*(_newswarm[i].pbest[j] - _newswarm[i].position[j] )
     + _C2*(rand()%101)/100.*(_gbest[j] - _newswarm[i].position[j]) );
	 	   
    _newswarm[i].position[j] = _newswarm[i].position[j] + _newswarm[i].v[j] ;

    if ( _newswarm[i].position[j] > _range ) _newswarm[i].position[j] = _range ;
    else if ( _newswarm[i].position[j] < -1*_range) _newswarm[i].position[j] = -1*_range ;
  }
 
  }
 
  for(i=1;i<=_swarmsize;i++)
  {
      fun =  Eva_cost_fun(i) ;
	  _newswarm[i].objfunc(fun) ;
	 statistics(&fmax_fun,&fmin_fun,&ave_fun,_newswarm[j].cost_fun,i,min) ;
	 
  }
  
 for( i=1; i<=_swarmsize;i++)
  if ( _newswarm[i].cost_fun < _newswarm[i].pbest_fun )
   {
      _newswarm[i].pbest_fun  = _newswarm[i].cost_fun ;

      for (j=1;j<=_lparticle;j++)
	_newswarm[i].pbest[j] = _newswarm[i].position[j] ;
   }
  
  for(j=1;j<=_swarmsize;j++)
    if ( _newswarm[j].cost_fun < _gbest_fun )
     { _gbest_fun = _newswarm[j].cost_fun ;
        for(i=1;i<=_lparticle;i++)
         _gbest[i] = _newswarm[j].position[i] ;
    
	}

    report_save_fun(fmax_fun,_gbest_fun,ave_fun,Cost_name) ;

}

void   report_save_fun(float max_fun,float g_min_fun,float ave_fun,char *p)
{
  FILE *fm ;
  errno_t err;

 cout << "\n max --- " << max_fun << " , g_min --- " << g_min_fun << " , avg -- " << ave_fun ;
 
    if( ( err=fopen_s(&fm, Cost_name,"a")) != 0) exit(1) ;
       fprintf(fm,"%f  %f  %f" ,max_fun,g_min_fun,ave_fun) ;

  fprintf(fm,"\n") ;
  
 fclose(fm);
  
}

void Save()
{

 int i;
 FILE *fm ;
 errno_t err;

 float fun ;
 
  if( (err=fopen_s(&fm, Final_swarm_name,"w")) != 0) exit(1) ;

   for(i=1;i<=_lparticle;i++)
        _newswarm[1].position[i] = _gbest[i]  ;
  
  _newswarm[1].Save_variable(fm) ;
  
  fclose(fm) ;
    
  fun =  Eva_cost_fun(1) ;

  cout << "\n final  ---- " << fun ;

}

void Swarm::Save_variable(FILE  *ft)
{
  int k ;
  
  for(k=1;k<=_lparticle;k++)
       fprintf(ft," %f ", position[k] ) ;

      fprintf(ft,"\n") ;

}