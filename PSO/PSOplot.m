close all;clear;clc;
cost_func=load('cost.dat');
particles_ini=load('initial_position.dat');
particles_fin=load('final_position.dat');

figure;plot(1:length(cost_func),log10(cost_func(:,1)));
% figure;plot(1:length(cost_func),cost_func(:,1));
title('PSOΘセㄧ计程');xlabel('舼Ω计');ylabel('log10[Θセㄧ计程]');
grid on;set(gca,'fontsize',20);

figure;plot(1:length(cost_func),log10(cost_func(:,2)));
% figure;plot(1:length(cost_func),cost_func(:,2));
title('PSO办程');xlabel('舼Ω计');ylabel('log10[办程]');
grid on;set(gca,'fontsize',20);

figure;plot(1:length(cost_func),log10(cost_func(:,3)));
% figure;plot(1:length(cost_func),cost_func(:,3));
title('PSOΘセㄧ计キА');xlabel('舼Ω计');ylabel('log10[Θセㄧ计キА]');
grid on;set(gca,'fontsize',20);

x=-4.5:0.01:4.5;y=-4.5:0.01:4.5;
for i=1:1: length(x)
    for j=1:1:length(y)
% z(i,j)=x(i)^2-10*cos(2*pi*x(i))+10+ y(j)^2-10*cos(2*pi*y(j))+10;    % 絛ㄒ        
z(i,j)=(1.5-x(i)+x(i)*y(j))^2+(2.25-x(i)+x(i)*y(j)^2)^2+(2.625-x(i)+x(i)*y(j)^3)^2; % 穨
    end
end
figure;contour(x,y,z);hold on;plot(particles_ini(:,1),particles_ini(:,2),'*');
hold on;plot(particles_fin(:,1),particles_fin(:,2),'*');
legend('Θセㄧ计单蔼絬瓜','﹍竚','程沧竚')
title('PSO秆丁');xlabel('跑计x');ylabel('跑计y');
grid on;set(gca,'fontsize',20);axis equal;